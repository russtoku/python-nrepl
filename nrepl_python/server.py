import selectors
import socket
import bencodepy
from collections import OrderedDict
import argparse
import datetime
import traceback
import sys

import operations

def debug_message(message):
    if Server.debug.lower() == "yes":
        now = datetime.datetime.now()
        print(f'<{now.strftime("%H:%M")}> {message}')

def convert(e):
    if isinstance(e, (dict, OrderedDict)):
        return {k.decode():convert(v.decode()) for k,v in e.items()}
    if isinstance(e, list):
        return [convert(v) for v in e]
    return e

sel = selectors.DefaultSelector()

def accept(sock, mask):
    conn, addr = sock.accept()
    debug_message(f'Accepted {conn} from {addr}')

    conn.setblocking(False)
    sel.register(conn, selectors.EVENT_READ, read)

def read(conn, mask):
    try:
        data = conn.recv(10000)  # Should be ready
        if data:
            message = convert(bencodepy.decode(data))

            debug_message(f'Recieve {message} from {conn}')

            response = eval(f'operations.op_{message.get("op").replace("-","_")}({message})')

            response_bytes = bencodepy.encode(response)

            debug_message(f'Returning {repr(response)} to {conn}')
            conn.sendall(response_bytes)

            if message.get("op") == 'close':
                print('Closing: ', conn)
                sel.unregister(conn)
                conn.close()

        else:
            print('Closing: ', conn)
            sel.unregister(conn)
            conn.close()

    except Exception as error:
        debug_message(f'Closing {conn}')
        debug_message(f'Unexpected error {error}')
        #traceback.print_stack()

        conn.close()
        sel.unregister(conn)

def run_s(ip,port):
    try:
        print(f"<Running NREPL Python in {ip}:{port}>")
        with socket.socket() as sock:
            
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.bind((ip, int(port)))

            sock.listen(100)
            sock.setblocking(False)
            
            sel.register(sock, selectors.EVENT_READ, accept)
            
            while True:
                events = sel.select()
                for key, mask in events:
                    callback = key.data
                    callback(key.fileobj, mask)
    except:
        sel.close()
        sock.shutdown(1)
        sock.close()

class Server: pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("-i","--ip")
    parser.add_argument("-p","--port")
    parser.add_argument("-d","--debug")

    parser.set_defaults(ip="localhost",port=7888,debug="no")

    args = parser.parse_args()

    Server.ip = args.ip
    Server.port = args.port
    Server.debug = args.debug

    try:
        run_s(Server.ip, Server.port)

    except:
        sys.exit()
