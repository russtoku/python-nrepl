import session
import tempfile
import os
import contextlib
import io
import ast
import datetime
import traceback
import importlib
import re
import sys
import jedi
import pydoc

import IPython

def _add_id(response,id):
    if id: response['id'] = id

    return response
        

def op_clone(message):

    response = {'status': 'started',
                'time-stamp' : str(datetime.datetime.now())}

    _add_id(response,message.get("id"))

    storage_session = session.sessions.get(message.get('session'))
    local_session = False

    if storage_session:
        clone = storage_session.clone()
        session.sessions[clone.id] = clone
        local_session = clone
        
    else: 
        local_session = session.Session()
        session.sessions[local_session.id] = local_session


    response['new-session'] = local_session.id

    return response

def op_describe(message):
    response = {'status': ['done'],
                'time-stamp' : str(datetime.datetime.now()),
                'server-capabilities': {
                    'ops': ['clone', 'describe','eval','complete',
                            'ls-sessions','load-file'],
                'ns': ['user']}}

    _add_id(response,message.get("id"))

    return response

def op_eval(message):
    response =  {'status': ['done'],
                 'time-stamp' : str(datetime.datetime.now())}
    _add_id(response,message.get("id"))

    result = eval_string(message.get("code"))
    if result: response['value'] = str(result)
                
    return response

def op_completions(message):
    prefix = message.get("prefix")

    completions = False

    response =  {'status':['done'],
                 'time-stamp' : str(datetime.datetime.now()),
                 'id':message.get('id')}

    if len(prefix) > 0 :
        completions = get_completions(prefix)

    if completions:
        response['completions'] = completions

    return response

def op_interrupt(message):
    pass

def op_load_file(message):
    response = {'status':['done'],
                'time-stamp' : str(datetime.datetime.now()),
                'id':message.get('id')}
    file_content = message.get("file")

    result = eval_string(file_content)
    if result: response['value'] = str(result)

    return response

def op_lookup(message):
    pass

def op_ls_sessions(message):
    response = {'status':['done'],
                'time-stamp' : str(datetime.datetime.now()),
                'id':message.get('id'),
                'sessions': list(session.sessions.keys())}

    return response

def op_sideload_provide(message):
    pass

def op_stdin(message):
    pass


# Middleware specific
def op_add_middleware(message):
    pass

def op_swap_middleware():
    pass

def op_ls_middleware(message):
    pass

def eval_string(text):
    tmp_file = tempfile.mkstemp()[1]
    io_output = False

    try:
        with contextlib.redirect_stdout(io.StringIO()) as f:
            result = _eval(text,tmp_file)
            io_output = f.getvalue()
    except Exception as e:
        result = ''.join(traceback.format_exception(None, e, e.__traceback__))

    os.remove(tmp_file)
    
    if io_output: result = f'{io_output}{result}'

    return result

def _eval(source, filename):
    try:
        p, e = ast.parse(source, filename), None
        if p.body and isinstance(p.body[-1], ast.Expr):
            e = p.body.pop()

        g = globals()
        exec(compile(p, filename, 'exec'), g, g)
        if e:
            return eval(compile(ast.Expression(e.value), filename, 'eval'), g, g)
    except Exception as e:
        raise e


def _completions(prefix):
    source = ""
    for p in re.split(r"\W+",prefix):
        g = globals().get(p)
        if g: source = f"{source}import {g.__name__}\n"
        
    source = f"{source}{prefix}"

    lines = source.splitlines()
    script = jedi.Script(code=source,project=jedi.get_default_project(sys.prefix),path='example.py')

    return [comp.name for comp in script.complete(len(lines),len(prefix))]
    

def _type(prefix,comp):
    try:
        return type(pydoc.locate(f'{prefix}.{comp}'))
    except:
        return "False"

def _readline_completer(text):
 import readline
 completer = readline.get_completer()
 completions = []

 import rlcompleter
 completer = readline.get_completer()

 if getattr(completer, 'PYTHON_EL_WRAPPED', False):
     completer.print_mode = False
 i = 0
 while True:
     completion = completer(text, i)
     if not completion:
         break
     i += 1
     completions.append(completion)

 if getattr(completer, 'PYTHON_EL_WRAPPED', False):
    completer.print_mode = True

 return completions

def get_completions(text):
    completions = []

    is_ipython =  importlib.util.find_spec("IPython")

    if is_ipython:
        try:
            completions = _completions(text)
        except: pass
    elif len(completions) == 0:
        completions = _readline_completer(text)
    else:
        return []
    
    return [{"candidate": comp,"type": str(_type(text,comp).__name__)} for comp in completions]
